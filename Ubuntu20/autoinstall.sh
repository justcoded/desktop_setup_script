#!/bin/bash

curl http://justcoded.co/adminscripts/templates/Chrome.tar.gz -o ~/.config/Chrome.tar.gz
if [ -d ~/.config/google-chrome ]
  then
    mv ~/.config/google-chrome ~/.config/google-chrome_`date +%F%H%M%S`
fi
cd ~/.config/
tar -zxvf Chrome.tar.gz
mv Chrome ~/.config/google-chrome

sudo mkdir /usr/share/backgrounds/old
sudo find /usr/share/backgrounds/  -type f -name "*.jpg" -exec mv {} /usr/share/backgrounds/old \;
cd /usr/share/backgrounds/
sudo wget -A png -nd -r --no-parent --reject "index.html*" http://justcoded.co/adminscripts/wallpapers/
find /usr/share/backgrounds/ -name "* *" -type f | sudo rename 's/ /_/g'

cd ~/
wget http://justcoded.co/adminscripts/linux/xmlgen.sh
chmod 777 xmlgen.sh
sudo ~/xmlgen.sh /usr/share/backgrounds/


cd ~/
wget http://justcoded.co/adminscripts/linux/install_apache2.sh
wget http://justcoded.co/adminscripts/linux/vhost_add.sh
chmod 777 install_apache2.sh
chmod 777 vhost_add.sh
sudo service mysql stop
sudo mv /var/lib/mysql /var/lib/mysql_old
./install_apache2.sh
./vhost_add.sh


INTERFACE=$(ip link show | grep "state UP" | grep enp | sed -e 's/://g'| awk '{print $2}')
if [ -n "$INTERFACE" ]
    then
        INTERFACES_CONFIG="
auto $INTERFACE
iface $INTERFACE inet dhcp"
         sudo sh -c "echo \"$INTERFACES_CONFIG\" >> /etc/network/interfaces"
fi
sudo service networking restart
rm ~/install_apache2.sh
rm ~/vhost_add.sh
rm ~/xmlgen.sh
