#!/bin/bash

set PC hostname
echo "Please enter new hostname: "
read HOSTNAME
sudo hostnamectl set-hostname "$HOSTNAME"
sudo apt-get install -y dnsmasq
sudo sed -i 's/#domain-needed/domain-needed/g' /etc/dnsmasq.conf
sudo sed -i 's/#bogus-priv/bogus-priv/g' /etc/dnsmasq.conf
sudo sed -i 's/#strict-order/strict-order/g' /etc/dnsmasq.conf
sudo sed -i 's/#listen-address=/listen-address=127.0.0.1/g' /etc/dnsmasq.conf
sudo sh -c "echo domain=$HOSTNAME >> /etc/dnsmasq.conf"
sudo sh -c "echo address=\/$HOSTNAME\/127.0.0.1 >> /etc/dnsmasq.conf"
sudo service dnsmasq restart

# apache / php
wget https://dev.mysql.com/get/mysql-apt-config_0.8.7-1_all.deb
sudo dpkg -i mysql-apt-config_0.8.7-1_all.deb
sudo add-apt-repository -y ppa:ondrej/php
sudo apt-get update
sudo apt-get install -y mysql-server mysql-client
sudo apt-get install -y apache2

for AVAILABLE_PHP in `apt-cache search php | grep -e "^php[0-9]\.[0-9] -" | sed 's/ -.*//g'`
do
   echo $AVAILABLE_PHP
    sudo apt-get install -y $AVAILABLE_PHP-{imap,common,gd,json,xsl,xmlrpc,curl,mysql,cli,dev,intl,ps,pspell,readline,recode,sqlite3,tidy,fpm,mbstring,opcache,xml,xs,zip}
    if ! [[ "$AVAILABLE_PHP" =~ php7.[2-9] ]]
        then
            sudo apt-get install -y $AVAILABLE_PHP-mcrypt
    fi
done
apt-get install php-xdebug
XDEBUG='
xdebug.remote_enable=1
xdebug.remote_host=localhost
xdebug.remote_port=9000
xdebug.remote_handler=dbgp
'

for PHP in `ls /etc/php`
do
    #setup xdebug
    sudo sh -c "echo \"$XDEBUG\" >> /etc/php/$PHP/mods-available/xdebug.ini"
    # enable error reporting in PHP
    sudo sed -i 's/display_errors = Off/display_errors = On/g' /etc/php/$PHP/apache2/php.ini
    sudo sed -i 's/display_errors = Off/display_errors = On/g' /etc/php/$PHP/cli/php.ini
    sudo sed -i 's/display_errors = Off/display_errors = On/g' /etc/php/$PHP/fpm/php.ini
    sudo service php$PHP-fpm restart
done

# enable apache modules: VirtualHost, ModRewrite
sudo a2enmod vhost_alias rewrite proxy proxy_fcgi
sudo a2dismod php5 php7.0 php7.1 php5.6  mpm_prefork
sudo a2enmod mpm_event
sudo service apaceh2 restart

# restart apache
sudo service apache2 restart

MAILSCRIPT='#!/bin/sh 
prefix=\"/var/mail/sendmail/new\"
numPath=\"/var/mail/sendmail\"
 
if [ ! -f \$numPath/num ]; then 
echo \"0\" > \$numPath/num 
fi 
num=\`cat \$numPath/num\` 
num=\$((\$num + 1)) 
echo \$num > \$numPath/num 
 
name=\"\$prefix/letter_\$num.txt\"
while read line 
do 
echo \$line >> \$name
done 
chmod 777 \$name
/bin/true'

sudo sh -c "echo \"$MAILSCRIPT\" >> /usr/bin/fakesendmail.sh"

sudo chown root:root /usr/bin/fakesendmail.sh
sudo chmod 0755 /usr/bin/fakesendmail.sh

sudo mkdir -p /var/mail/sendmail/cur /var/mail/sendmail/new /var/mail/sendmail/tmp
sudo chmod -R 0777 /var/mail/sendmail

# update php.ini configs
sudo sed -i 's/;sendmail_path =/sendmail_path = \/usr\/bin\/fakesendmail.sh/g' /etc/php5/apache2/php.ini
sudo sed -i 's/;sendmail_path =/sendmail_path = \/usr\/bin\/fakesendmail.sh/g' /etc/php5/cli/php.ini

sudo service apache2 restart

# install mail viewer
sudo apt-get install -y evolution

echo ''
echo ''
echo '  >>  NOW YOU WILL NEED TO SETUP Evolution software to view you mail  << '
echo '  1. In the opened Evolution software set your name and email like "mail@localhost.com"'
echo '  2. Receiving Email options: '
echo '     2.1 Server Type: Maildir-format mail directories'
echo '     2.2 Mail Directory: /var/mail/sendmail'
echo '  3. Sending Email:   choose "Sendmail" as Server Type'
echo '  4. Give you mailbox some name like "Localhost mail"'
echo '  5. Ignore and close red warning when you try to open mailbox.'

read -s -n 1 -p "Now we will open this software for you. Press any key to continue..."
echo ''
echo '  ... opening evolution software...'

# launch evolution mail program
evolution

# just message at the end
read -s -n 1 -p "All is done. Press any key to continue..."
echo
