#!/bin/bash

echo "Please enter new vhost added: "
read DOMAIN

echo "Please enter your username (used to chown folders): "
read USERNAME

echo "Wildcard domains should point to separate folder? Default: Yes (y/n)"
read -n 1 WILDCARD
echo ' '

#sudo a2enmod vhost_alias

# create vhost directory, index.html and chmod/chown
sudo mkdir -p "/var/vhosts/$DOMAIN/httpdocs"
sudo mkdir -p "/var/vhosts/$DOMAIN/.logs"
sudo chmod 0777 -R "/var/vhosts/$DOMAIN"
sudo chown $USERNAME:$USERNAME -R "/var/vhosts/$DOMAIN"
sudo sh -c "echo \"<h1>Your domain is $DOMAIN. If you see this - you rock!</h1>\" >> /var/vhosts/$DOMAIN/httpdocs/index.html"
echo "  >> Created /var/vhosts/$DOMAIN/httpdocs and .logs folders"

CONF_FILENAME=/etc/apache2/sites-available/$DOMAIN.conf

for PHP in `ls /etc/php`
    do 
        VERSION=$(echo $PHP | sed -e 's/\.//;s/^/p/')
        CONF_FILENAME=/etc/apache2/sites-available/$VERSION.$DOMAIN.conf
        ONE_FOLDER_CONFIG="
<VirtualHost *:80>
        ServerName $VERSION.$DOMAIN
        ServerAlias *.$VERSION.$DOMAIN
        ServerAdmin webmaster@localhost

        DocumentRoot /var/vhosts/$DOMAIN/httpdocs
        <Directory \"/var/vhosts/$DOMAIN/httpdocs\">
                Options Indexes FollowSymLinks MultiViews
                AllowOverride All
                Require all granted
                <FilesMatch \.php$>
                    <IfModule proxy_module>
                        SetHandler proxy:unix:///var/run/php/php$PHP-fpm.sock|fcgi://127.0.0.1:9000
                    </IfModule>
                </FilesMatch>
        </Directory>

        ErrorLog /var/vhosts/test.pers/.logs/error_"$PHP".main.log
        CustomLog /var/vhosts/test.pers/.logs/access_"$PHP".main.log combined
</VirtualHost>"

MULTI_FOLDER_CONFIG="
<VirtualHost *:80>
        ServerAlias *.$VERSION.$DOMAIN
        ServerAdmin webmaster@localhost
        
        VirtualDocumentRoot /var/vhosts/$DOMAIN/httpdocs/%1
        <Directory \"/var/vhosts/$DOMAIN\">
                Options Indexes FollowSymLinks MultiViews
                AllowOverride All
                Require all granted
                <FilesMatch \.php$>
                    <IfModule proxy_module>
                        SetHandler proxy:unix:///var/run/php/php$PHP-fpm.sock|fcgi://127.0.0.1:9000
                    </IfModule>
                </FilesMatch>
        </Directory>

        ErrorLog /var/vhosts/$DOMAIN/.logs/error_"$PHP".wildcard.log
        CustomLog /var/vhosts/$DOMAIN/.logs/access_"$PHP".wilcard.log combined
</VirtualHost>
<VirtualHost *:80>
        ServerName $VERSION.$DOMAIN
        ServerAlias www.$VERSION.$DOMAIN
        ServerAdmin webmaster@localhost

        DocumentRoot /var/vhosts/$DOMAIN/httpdocs
        <Directory \"/var/vhosts/$DOMAIN/httpdocs\">
                Options Indexes FollowSymLinks MultiViews
                AllowOverride All
                Require all granted
                <FilesMatch \.php$>
                    <IfModule proxy_module>
                        SetHandler proxy:unix:///var/run/php/php$PHP-fpm.sock|fcgi://127.0.0.1:9000
                    </IfModule>
                </FilesMatch>
                </Directory>

        ErrorLog /var/vhosts/$DOMAIN/.logs/error_"$PHP".main.log
        CustomLog /var/vhosts/$DOMAIN/.logs/access_"$PHP".main.log combined
</VirtualHost>"

        if ( [ "$WILDCARD" == "n" ] || [ "$WILDCARD" == "N" ] ); 
        then 
            sudo sh -c "echo \"$ONE_FOLDER_CONFIG\" >> $CONF_FILENAME"
        else
            sudo sh -c "echo \"$MULTI_FOLDER_CONFIG\" >> $CONF_FILENAME"
        fi
        echo "  >> Created $CONF_FILENAME vhost file"
        sudo a2ensite $VERSION.$DOMAIN
    done
# activate config
sudo service apache2 restart
echo "  >> Apache2 restarted"
echo "  >> That's all forks!"

