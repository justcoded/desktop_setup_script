#!/bin/bash
xcode-select --install
sudo xcodebuild -license
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
git -C "$(brew --repo homebrew/core)" fetch --unshallow
brew tap caskroom/cask
brew cleanup 
brew install brew-cask
brew cask install java
brew cask install lightshot
brew cask install thunderbird
brew cask install firefox
brew cask install google-chrome
brew cask install adobe-acrobat-reader
brew install node
brew cask install virtualbox
brew cask install libreoffice
brew cask install sublime-text
brew cask install slack
brew cask install skype
brew cask install osxfuse
brew cask install sshfs
brew cask install pencil
brew cask install sequel-pro
brew install wget
/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome
killall "Google Chrome"
curl http://justcoded.co/adminscripts/templates/Chrome.tar.gz -o ~/Library/Application\ Support/Google/Chrome.tar.gz
if [ -d ~/Library/Application\ Support/Google/Chrome ]
  then
    mv ~/Library/Application\ Support/Google/Chrome ~/Library/Application\ Support/Google/Chrome_`date +%F%H%M%S`
fi
cd ~/Library/Application\ Support/Google/
tar -jxvf Chrome.tar.gz

sudo mkdir /Library/Desktop\ Pictures/old
sudo find /Library/Desktop\ Pictures/  -type f -name "*.jpg" -exec mv {} /Library/Desktop\ Pictures/old/ \;
cd /Library/Desktop\ Pictures/
sudo wget -A png -nd -r --no-parent --reject "index.html*" http://justcoded.co/adminscripts/wallpapers/
